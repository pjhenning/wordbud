# wordbud

**IMPORTANT:** In order to run this program, you must have Java 1.8 (aka Java 8) installed on your system.

**To try the app**: run `./gradlew run`

### Important files:
- **Application entry point**: `src/main/kotlin/wordbud/BudApp.kt`
- **NLP-related logic**: `process-lib/src/main/scala/process/Library.scala`