package process

import java.net.URL
import java.util
import java.util.Properties

import edu.mit.jwi._
import edu.mit.jwi.data.ILoadPolicy
import edu.mit.jwi.item.{ISynset, POS, Pointer}
import edu.stanford.nlp.coref.data.CorefChain
import edu.stanford.nlp.ie.util._
import edu.stanford.nlp.ling._
import edu.stanford.nlp.naturalli.NaturalLogicAnnotations
import edu.stanford.nlp.pipeline._
import edu.stanford.nlp.semgraph._
import edu.uci.ics.jung.algorithms.layout.{DAGLayout, Layout}
import edu.uci.ics.jung.graph.DirectedSparseMultigraph
import edu.uniba.di.lacam.kdde.lexical_db.MITWordNet
import edu.uniba.di.lacam.kdde.lexical_db.data.Concept
import edu.uniba.di.lacam.kdde.ws4j.similarity.LeacockChodorow
import simplenlg.features.{Feature, NumberAgreement, Tense}
import simplenlg.framework.NLGFactory
import simplenlg.lexicon.Lexicon
import simplenlg.realiser.english.Realiser

import scala.collection.mutable
import scala.jdk.CollectionConverters._

class SCNLPDocument() {
	private var doc: Option[CoreDocument] = None
	private var pipeline: Option[StanfordCoreNLP] = None

	def init_pipeline(): Unit = {
		val props = new Properties
		props.setProperty(
			"annotators",
			"tokenize,ssplit,pos,lemma,ner,parse,depparse,natlog,coref,openie"
		)
		props.setProperty("openie.resolve_coref", "true")
		props.setProperty("coref.algorithm", "neural")
		pipeline = Some(new StanfordCoreNLP(props))
	}

	def with_text(txt: String): Unit = {
		doc = Some(new CoreDocument(txt))
		pipeline.get.annotate(doc.get)
		StanfordCoreNLP.clearAnnotatorPool()
	}

	def getDep: List[SemanticGraph] = {
		doc.get.sentences.asScala.toList.map((sent) => sent.dependencyParse)
	}

	def getIE: List[List[RelationTriple]] = {
		val ann = doc.get.annotation
		val sentences =
			ann.get(classOf[CoreAnnotations.SentencesAnnotation]).asScala.toList
		sentences.map((sentence) => {
			sentence
				.get(classOf[NaturalLogicAnnotations.RelationTriplesAnnotation])
				.asScala
				.toList
		})
	}

	def getCoref: util.Map[Integer, CorefChain] = {
		doc.get.corefChains
	}
}

class VocabHelper() {
	private val bufferedSource = io.Source.fromFile(
		"process-lib/src/main/resources/DaleChallEasyWordList.txt"
	)
	private val words = (for (line <- bufferedSource.getLines) yield line).toArray
	bufferedSource.close

	def isSimple(word: String): Boolean = words.contains(word)
}

class WordNetHelper() {
	val dict =
		new RAMDictionary(
			new URL(
				"file",
				null,
				"process-lib/src/main/resources/WordNet/dict"
			),
			ILoadPolicy.BACKGROUND_LOAD
		)
	dict.open()

	def getSynonyms(sid: String): List[String] = {
		val tsid = item.SynsetID.parseSynsetID(sid)
		val synset = dict.getSynset(tsid)
		val hypernyms = synset.getRelatedSynsets(Pointer.HYPERNYM).asScala
		val hflat = hypernyms
			.flatMap(sid => dict.getSynset(sid).getWords.asScala.map(w => w.getLemma))
			.toList
		val synonyms = synset.getWords.asScala.map(word => word.getLemma).toList
		List.concat(hflat, synonyms)
	}

	def getSynsets(lemma: String, pos: item.POS): List[ISynset] = {
		val ind = dict.getIndexWord(lemma, pos)
		val wordIDs = ind.getWordIDs.asScala.toList
		val words = wordIDs.map(wid => dict.getWord(wid))
		words.map(word => word.getSynset)
	}
}

object sem {

	abstract class Node

	case class Entity(name: String) extends Node

	case class Attribute(name: String) extends Node

	def getNodeDisplayString(n: Node): String = n match {
		case Entity(name) =>
			name
		case Attribute(name) =>
			name
	}

	class NodeCandidate(val name: String)

	class Relation(val name: String)

	type SemTuple = (sem.Relation, sem.Node, sem.Node)

	class SemData(val dat: List[SemTuple])

}

object process {
	def computeText(txt: String, doc: SCNLPDocument): sem.SemData = {
		doc.with_text(txt)
		val triples = doc.getIE
		getEdges(triples)
	}

	def getEdges(triplesSets: List[List[RelationTriple]]): sem.SemData = {
		var edges: Set[sem.SemTuple] = Set()
		val wnh = new WordNetHelper
		triplesSets.foreach(triplesSet => {
			triplesSet.foreach(triple => {
				val selectLabel = (labels: util.List[CoreLabel]) => {
					var rLabel = labels.get(0)
					if (labels.size > 1) {
						// TODO: Make this more nuanced
						labels.asScala.foreach(label => {
							if (label.tag != "DT") {
								rLabel = label
							}
						})
					}
					rLabel
				}
				val subj = selectLabel(triple.canonicalSubject)
				val subjTag = subj.tag
				val relLemma = triple.relationLemmaGloss
				val relTag = selectLabel(triple.relation).tag
				val subjSimpBase = getSimple(
					wnh,
					subj.lemma,
					tagToPos(subjTag),
					relLemma,
					tagToPos(relTag)
				)
				val subjSimp = realize(subjSimpBase, tagToPos(subjTag), subjTag)
				val aNode = sem.Entity(subjSimp)
				val obj = selectLabel(triple.canonicalObject)
				val objTag = obj.tag
				val objSimpBase = getSimple(
					wnh,
					obj.lemma,
					tagToPos(objTag),
					relLemma,
					tagToPos(relTag)
				)
				val objSimp = realize(objSimpBase, tagToPos(objTag), objTag)
				val bNode = sem.Entity(objSimp)
				val simpBase = getSimple(
					wnh,
					relLemma,
					tagToPos(relTag),
					obj.lemma,
					tagToPos(obj.tag)
				)
				val simpRel = realize(simpBase, tagToPos(relTag), relTag)
				val rel = new sem.Relation(simpRel)
				val newEdge: sem.SemTuple = (rel, aNode, bNode)
				if (notAlreadyAnEdge(newEdge, edges))
					edges = edges + newEdge
			})
		})
		new sem.SemData(edges.toList)
	}

	def notAlreadyAnEdge(
			newEdge: sem.SemTuple,
			edges: Set[sem.SemTuple]
	): Boolean = {
		val (nR, nA, nB) = newEdge
		edges.forall((edge: sem.SemTuple) => {
			val (r, a, b) = edge
			(r.name != nR.name) && (sem.getNodeDisplayString(a) != sem
				.getNodeDisplayString(nA)) && (sem.getNodeDisplayString(b) != sem
				.getNodeDisplayString(nB))
		})
	}

	def realize(lemma: String, pos: Option[POS], tag: String): String = {
		val lex = Lexicon.getDefaultLexicon
		val nlgFactory = new NLGFactory(lex)
		val realiser = new Realiser(lex)
		pos match {
			case Some(POS.VERB) => {
				tag.last match {
					case 'D' =>
						val phr = nlgFactory.createVerbPhrase()
						phr.setVerb(lemma)
						phr.setFeature(Feature.TENSE, Tense.PAST)
						realiser.realise(phr).toString
					case _ => lemma
				}
			}
			case Some(POS.NOUN) => {
				val phr = nlgFactory.createNounPhrase()
				phr.setNoun(lemma)
				tag.last match {
					case 'S' =>
						phr.setFeature(Feature.NUMBER, NumberAgreement.PLURAL)
						realiser.realise(phr).toString
					case 'P' | 'N' =>
						phr.setFeature(Feature.NUMBER, NumberAgreement.SINGULAR)
						realiser.realise(phr).toString
					case _ => lemma
				}
			}
			case Some(POS.ADJECTIVE) | Some(POS.ADVERB) | None => lemma
		}
	}

	def prepGraph(semGraph: sem.SemData): Layout[sem.Node, sem.Relation] = {
		val sgg = new DirectedSparseMultigraph[sem.Node, sem.Relation]
		semGraph.dat.foreach(semTuple => {
			val (rel, nodeA, nodeB) = semTuple
			sgg.addEdge(rel, nodeA, nodeB)
		})
		new DAGLayout(sgg)
	}

	def tagToPos(tag: String): Option[item.POS] = {
		tag.charAt(0) match {
			case 'J' => Some(POS.ADJECTIVE)
			case 'N' => Some(POS.NOUN)
			case 'R' => Some(POS.ADVERB)
			case 'V' => Some(POS.VERB)
			case _ => {
				println(s"Tag '$tag' has no corresponding POS.")
				None
			}
		}
	}

	def getSimple(
			wnh: WordNetHelper,
			lemma: String,
			pos: Option[item.POS],
			partner: String,
			partnerPos: Option[item.POS]
	): String = {
		val vh = new VocabHelper
		if (vh.isSimple(lemma) || pos.isEmpty || partnerPos.isEmpty) {
			lemma
		} else {
			val set = getSimpleSet(wnh, lemma, pos.get, partner, partnerPos.get)
			if (set.nonEmpty) set.head else lemma
		}
	}

	def synsetToConcept(lemmaPos: POS, synset: ISynset): Concept = {
		val id = synset.getID.toString
		val pos = jwiToWs4Pos(lemmaPos)
		new Concept(id, pos)
	}

	def getSimpleSet(
			wnh: WordNetHelper,
			lemma: String,
			lemmaPos: item.POS,
			partner: String,
			partnerPos: item.POS
	): List[String] = {
		val lemmaConcepts =
			wnh.getSynsets(lemma, lemmaPos).map(synsetToConcept(lemmaPos, _))
		val partnerConcepts =
			wnh.getSynsets(partner, partnerPos).map(synsetToConcept(lemmaPos, _))
		// via https://stackoverflow.com/questions/11803349/composing-a-list-of-all-pairs
		val pairs = for (x <- lemmaConcepts; y <- partnerConcepts) yield (x, y)
		val ranked = mutable.SortedMap[Double, (Concept, Concept)]()
		val db = new MITWordNet(wnh.dict)
		val rc = new LeacockChodorow(db)
		pairs.foreach(pair => {
			val (lemma, partner) = pair
			val rel = rc.calcRelatednessOfSynsets(lemma, partner)
			val score = rel.getScore
			ranked(score) = pair
		})
		val (_, (selectedConcept, _)) = ranked.tail.toList.head
		val sid = selectedConcept.getSynsetID
		val vh = new VocabHelper
		val synonyms = wnh.getSynonyms(sid)
		synonyms.filter(vh.isSimple)
	}

	def jwiToWs4Pos(
			pos: edu.mit.jwi.item.POS
	): edu.uniba.di.lacam.kdde.lexical_db.item.POS = {
		edu.uniba.di.lacam.kdde.lexical_db.item.POS.values()(pos.ordinal)
	}
}
