package wordbud

import edu.uci.ics.jung.algorithms.layout.Layout
import edu.uci.ics.jung.visualization.VisualizationViewer
import edu.uci.ics.jung.visualization.control.GraphMouseListener
import javafx.embed.swing.SwingNode
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.control.TextArea
import process.SCNLPDocument
import process.process
import process.sem
import tornadofx.*
import java.awt.Dimension
import java.awt.event.MouseEvent
import java.util.function.Function
import kotlin.String
import kotlin.Unit
import com.google.common.base.Function as GFunction


class BudApp : App(BudView::class)

class BudView : View() {
	private val tv = TopView()
	override val root = gridpane {
		gridLinesVisibleProperty().set(true)
		row {
			this += tv
		}
		row {
			this += BottomView(tv::doGraph)
		}
	}
}

class TopView : View() {
	private lateinit var vv: VisualizationViewer<sem.Node, sem.Relation>
	private val n: SwingNode = SwingNode()
	private val p: ScrollPane = ScrollPane()

	init {
		p.prefHeight = 300.0
		p.isPannable = true
	}

	fun doGraph(data: sem.SemData) {
		val layout = process.prepGraph(data)
		showLayout(layout)
	}

	private fun showLayout(layout: Layout<sem.Node, sem.Relation>) {
		val dim = Dimension(550, 300)
		vv = VisualizationViewer(layout, dim)
		val eStringer =
					toGuavaFunc(Function<sem.Relation?, String> { e: sem.Relation? -> e!!.name() })
		val vStringer =
					toGuavaFunc(Function<sem.Node?, String> { v: sem.Node? -> sem.getNodeDisplayString(v) })
		vv.renderContext.edgeLabelTransformer = eStringer
		vv.renderContext.vertexLabelTransformer = vStringer
		//val gml = CGraphMouseListener<sem.Node>()
		//vv.addGraphMouseListener(gml)
		n.content = vv
		p.add(n)
	}

	private fun <T, R> toGuavaFunc(func: Function<T?, R>): GFunction<T, R>? {
		return GFunction { t: T? -> func.apply(t) }
	}

	internal class CGraphMouseListener<N> : GraphMouseListener<N> {
		override fun graphClicked(v: N, me: MouseEvent) {
			System.err.println("Node " + v + " was clicked at (" + me.x + "," + me.y + ")")
		}

		override fun graphPressed(v: N, me: MouseEvent) {
			System.err.println("Node " + v + " was pressed at (" + me.x + "," + me.y + ")")
		}

		override fun graphReleased(v: N, me: MouseEvent) {
			System.err.println("Node " + v + " was released at (" + me.x + "," + me.y + ")")
		}
	}

	override val root = p
}


class BottomView(graphFun: (t: sem.SemData) -> Unit) : View() {
	private var texBox: TextArea by singleAssign()
	private var lab: Label by singleAssign()
	private var butt: Button by singleAssign()
	private var doc: SCNLPDocument by singleAssign()

	override val root = vbox {
		doc = SCNLPDocument()
		texBox = textarea("I consumed a banana. It tasted good.")
		lab = label("Initializing...")
		butt = button("Analyze") {
			isVisible = false
			action {
				lab.text = "Loading..."
				runAsync {
					process.computeText(texBox.text, doc)
				} ui { semData ->
					graphFun(semData)
					println("Done :)")
					lab.text = "Done"
				}
			}
		}
		runAsync {
			doc.init_pipeline()
		} ui {
			println("Ready!")
			butt.isVisible = true
			lab.text = "Waiting for input"
		}
	}
}
